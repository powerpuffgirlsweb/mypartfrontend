import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor( public rutas:Router) { }

  ngOnInit() {
  }

  sign(){

    this.rutas.navigateByUrl("/sign");
  }

  login(){

    this.rutas.navigateByUrl("/login");
  }

}
