export class Activity {
     constructor(public id, public title, public description, public responsable, public team, public expdate, public donedate, public status){

     }

     changeStatus(){
         this.status = !this.status;
     }
}
