import { MYPARTPage } from './app.po';

describe('mypart App', () => {
  let page: MYPARTPage;

  beforeEach(() => {
    page = new MYPARTPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
