import { Component } from '@angular/core';
import {RouterModule,Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  constructor( private ruta:Router) { 

  this.rutas(); 
}


rutas(){
   this.ruta.navigateByUrl("/escritorio");
}
}


