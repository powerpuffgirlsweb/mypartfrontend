import { Component, OnInit } from '@angular/core';
import { ActivityService } from "services/activity";
import { Activity } from "models/activity";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {



  //actividades: Activity[] =[];

  actividades: any = [];

  constructor(public activityService: ActivityService) { }

  ngOnInit() {
    this.activityService.get().then(actividades =>{
       console.log(actividades);
       this.actividades = actividades;
    }).catch(error => console.log(error));
  }

}
