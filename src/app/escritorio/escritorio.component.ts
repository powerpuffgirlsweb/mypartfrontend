import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-escritorio',
  templateUrl: './escritorio.component.html',
  styleUrls: ['./escritorio.component.css']
})
export class EscritorioComponent{

 viewing: String = "home";

  getChange(newContentView){
    this.viewing = newContentView;
  }

}
