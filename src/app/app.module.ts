import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignComponent } from './sign/sign.component';
import { MenuComponent } from './menu/menu.component';
import {RouterModule,Routes} from '@angular/router';
import { AsideComponent } from './aside/aside.component';
import { ContentComponent } from './content/content.component';
import { HomeComponent } from './home/home.component';
import { TareasComponent } from './tareas/tareas.component';
import { EquiposComponent } from './equipos/equipos.component';
import { EscritorioComponent } from './escritorio/escritorio.component';

import { ActivityService } from '../services/activity';

const rutas: Routes=[
{ path: "sign", component:SignComponent},
{ path: "login", component:LoginComponent},
{ path: "escritorio", component:EscritorioComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignComponent,
    MenuComponent,
    AsideComponent,
    ContentComponent,
    HomeComponent,
    TareasComponent,
    EquiposComponent,
    EscritorioComponent
  ],
  imports: [

    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rutas)
  ],
  providers: [ActivityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
