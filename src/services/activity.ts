import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs';

import { config } from "./config";


@Injectable()
export class ActivityService {

  constructor( private http: Http) { }

  get(){
    let url = config.SERVICES+"Activities/index";
    return this.http.get(url).toPromise().then(actividades => actividades.json());
  }

}
